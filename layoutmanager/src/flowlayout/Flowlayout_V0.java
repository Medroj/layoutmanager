package flowlayout;
//
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Flowlayout_V0 extends JFrame {

	private JPanel contentPane;
	private JTextField txtSchreibWas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Flowlayout_V0 frame = new Flowlayout_V0();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Flowlayout_V0() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnHierIstEin = new JButton("Button1");
		btnHierIstEin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JLabel lblNeuesLable = new JLabel("Neues Lable");
		contentPane.add(lblNeuesLable);
		
		JButton btnButton = new JButton("Button0");
		contentPane.add(btnButton);
		contentPane.add(btnHierIstEin);
		
		txtSchreibWas = new JTextField();
		txtSchreibWas.setText("Schreib was!");
		contentPane.add(txtSchreibWas);
		txtSchreibWas.setColumns(10);
	}

}
